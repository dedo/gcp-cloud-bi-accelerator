CREATE TABLE IF NOT EXISTS customer (
    customer_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_number VARCHAR(15) NOT NULL,
    last_name VARCHAR(256) NOT NULL,
    first_name VARCHAR(256) NOT NULL,
    alternate_lastName VARCHAR(256),
    salutation VARCHAR(10),
    birth_date DATE,
    gender VARCHAR(1),
    marital_status VARCHAR(16),
    anniversary DATE,
    primary_email VARCHAR(256),
    secondary_email VARCHAR(256),
    primary_phone VARCHAR(32),
    primary_phone_type VARCHAR(32),
    secondary_phone VARCHAR(32),
    secondary_phone_type VARCHAR(32),
    employee_flag BOOLEAN,
    social_handle_1 VARCHAR(256),
    social_handle_1_type VARCHAR(32),
    social_handle_2 VARCHAR(256),
    social_handle_2_type VARCHAR(32),
    household_id BIGINT,
    PRIMARY KEY (customer_id)
);

CREATE TABLE IF NOT EXISTS customer_relationship (
    customer_relationship_id BIGINT NOT NULL AUTO_INCREMENT,
    source_customer_id BIGINT NOT NULL,
    target_customer_id BIGINT NOT NULL,
    relationship_type VARCHAR(64) NOT NULL,
    PRIMARY KEY (customer_relationship_id)
);

ALTER TABLE customer_relationship ADD CONSTRAINT  customer_relationship_fk1 FOREIGN KEY (source_customer_id) REFERENCES customer(customer_id);
ALTER TABLE customer_relationship ADD CONSTRAINT  customer_relationship_fk2 FOREIGN KEY (target_customer_id) REFERENCES customer(customer_id);

CREATE TABLE customer_address (
    address_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    address_line_1 VARCHAR(1024) NOT NULL,
    address_line_2 VARCHAR(1024),
    city VARCHAR(256) NOT NULL,
    state VARCHAR(256) NOT NULL,
    postal_code VARCHAR(16) NOT NULL,
    country VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (address_id)
);

ALTER TABLE customer
    ADD created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ADD updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

ALTER TABLE customer_relationship
    ADD created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ADD updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

ALTER TABLE customer_address ADD CONSTRAINT customer_address_fk1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

CREATE TABLE customer_segment (
    customer_segment_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    customer_segment_name VARCHAR(256) NOT NULL,
    customer_segment_value VARCHAR(256),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (customer_segment_id)
);

ALTER TABLE customer_segment ADD CONSTRAINT customer_segment_fk1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

CREATE TABLE customer_loyalty (
    customer_loyalty_id BIGINT NOT NULL AUTO_INCREMENT,
    loyalty_number VARCHAR(16) NOT NULL,
    loyalty_join_date DATE,
    loyalty_lifetime_points BIGINT DEFAULT 0,
    loyalty_available_points BIGINT,
    loyalty_level SMALLINT,
    loyalty_level_description VARCHAR(64),
    loyalty_level_expiration DATE,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (customer_loyalty_id)
);

ALTER TABLE customer_loyalty
    ADD customer_id BIGINT NOT NULL;
ALTER TABLE customer_loyalty ADD CONSTRAINT customer_loyalty_fk1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

CREATE TABLE customer_preferences (
    customer_preference_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    custoer_preference_type VARCHAR(64) NOT NULL,
    customer_preference_value VARCHAR(1024) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (customer_preference_id)
);

ALTER TABLE customer_preferences ADD CONSTRAINT customer_preferences_fk1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

CREATE TABLE store (
    store_id BIGINT NOT NULL AUTO_INCREMENT,
    store_num VARCHAR(16) NOT NULL,
    store_name VARCHAR(64) NOT NULL,
    store_address_line_1 VARCHAR(1024) NOT NULL,
    store_address_line_2 VARCHAR(1024),
    store_city VARCHAR(256) NOT NULL,
    store_state VARCHAR(256) NOT NULL,
    store_postal_code VARCHAR(16) NOT NULL,
    store_country VARCHAR(256) NOT NULL,
    latitude DECIMAL(11,8) NOT NULL,
    longitude DECIMAL(11,8) NOT NULL,
    store_phone_number VARCHAR(32) NOT NULL,
    open_date DATE,
    close_date DATE,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (store_id)
);

CREATE TABLE employee (
    employee_id BIGINT NOT NULL AUTO_INCREMENT,
    employee_number VARCHAR(10) NOT NULL,
    employee_first_name VARCHAR(256) NOT NULL,
    employee_last_name VARCHAR(256) NOT NULL,
    employee_type VARCHAR(32) NOT NULL,
    employee_store_id BIGINT NOT NULL,
    employee_department_id MEDIUMINT,
    employee_level VARCHAR(32),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (employee_id)
);


ALTER TABLE employee ADD CONSTRAINT employee_fk1 FOREIGN KEY (employee_store_id) REFERENCES store(store_id);


CREATE TABLE customer_employee_relationship (
    employee_relationship_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    employee_id BIGINT NOT NULL,
    relationship_type VARCHAR(64),
    relationship_start_date DATE,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (employee_relationship_id)
);

ALTER TABLE customer_employee_relationship ADD CONSTRAINT foreign key (customer_id) REFERENCES customer(customer_id);
ALTER TABLE customer_employee_relationship ADD CONSTRAINT foreign key (employee_id) REFERENCES employee(employee_id);

CREATE TABLE channel (
    channel_id SMALLINT NOT NULL AUTO_INCREMENT,
    channel_name VARCHAR(64) NOT NULL,
    PRIMARY KEY (channel_id)
);

CREATE TABLE vendor (
    vendor_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    vendor_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (vendor_id)
);

CREATE TABLE division (
    division_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    division_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (division_id)
);

CREATE TABLE department (
    department_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    division_id MEDIUMINT NOT NULL,
    department_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (department_id)
);

ALTER TABLE department ADD CONSTRAINT department_fk1 FOREIGN KEY (division_id) REFERENCES  division(division_id);
ALTER TABLE employee ADD CONSTRAINT employee_fk2 FOREIGN KEY (employee_department_id) REFERENCES department(department_id);

CREATE TABLE classification (
    classification_id BIGINT NOT NULL AUTO_INCREMENT,
    department_id MEDIUMINT NOT NULL,
    classification_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (classification_id)
);

ALTER TABLE classification ADD CONSTRAINT classification_fk1 FOREIGN KEY (department_id) REFERENCES  department(department_id);

CREATE TABLE sub_classification (
    sub_classification_id BIGINT NOT NULL AUTO_INCREMENT,
    classification_id BIGINT NOT NULL,
    sub_classification_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (sub_classification_id)
);

ALTER TABLE sub_classification ADD CONSTRAINT sub_classification_fk1 FOREIGN KEY (classification_id) REFERENCES  classification(classification_id);

CREATE TABLE style (
    style_id BIGINT NOT NULL AUTO_INCREMENT,
    sub_classification_id BIGINT NOT NULL,
    style_name VARCHAR(256) NOT NULL,
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (style_id)
);

ALTER TABLE style ADD CONSTRAINT style_fk1 FOREIGN KEY (sub_classification_id) REFERENCES  sub_classification(sub_classification_id);

CREATE TABLE item (
    item_id BIGINT NOT NULL AUTO_INCREMENT,
    vendor_id MEDIUMINT NOT NULL,
    division_id MEDIUMINT NOT NULL,
    department_id MEDIUMINT NOT NULL,
    classification_id BIGINT NOT NULL,
    sub_classification_id BIGINT NOT NULL,
    style_id BIGINT NOT NULL,
    sku VARCHAR(64) NOT NULL,
    color VARCHAR(32),
    size VARCHAR(32),
    short_description VARCHAR(2048) NOT NULL,
    long_description TEXT,
    image_url VARCHAR(1024),
    current_price DECIMAL(8,2),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (item_id)
);

ALTER TABLE item ADD CONSTRAINT item_fk1 FOREIGN KEY (vendor_id) REFERENCES  vendor(vendor_id);
ALTER TABLE item ADD CONSTRAINT item_fk2 FOREIGN KEY (division_id) REFERENCES  division(division_id);
ALTER TABLE item ADD CONSTRAINT item_fk3 FOREIGN KEY (department_id) REFERENCES  department(department_id);
ALTER TABLE item ADD CONSTRAINT item_fk4 FOREIGN KEY (classification_id) REFERENCES  classification(classification_id);
ALTER TABLE item ADD CONSTRAINT item_fk5 FOREIGN KEY (sub_classification_id) REFERENCES  sub_classification(sub_classification_id);
ALTER TABLE item ADD CONSTRAINT item_fk6 FOREIGN KEY (style_id) REFERENCES  style(style_id);

CREATE TABLE customer_favorites (
    customer_favorite_id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    vendor_id MEDIUMINT,
    category_id BIGINT,
    item_id BIGINT,
    source VARCHAR(256),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (customer_favorite_id)
);

ALTER TABLE customer_favorites ADD CONSTRAINT customer_favorites_fk1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);
ALTER TABLE customer_favorites ADD CONSTRAINT customer_favorites_fk2 FOREIGN KEY (vendor_id) REFERENCES vendor(vendor_id);
ALTER TABLE customer_favorites ADD CONSTRAINT customer_favorites_fk3 FOREIGN KEY (item_id) REFERENCES item(item_id);

CREATE TABLE sales_header (
    order_id BIGINT NOT NULL AUTO_INCREMENT,
    channel_id SMALLINT NOT NULL,
    customer_id BIGINT,
    primary_associate_id BIGINT,
    secondary_associate_id BIGINT,
    total_amount DECIMAL(8,2),
    store_id BIGINT,
    pos_terminal_id VARCHAR(32),
    transaction_date DATE NOT NULL,
    transaction_number VARCHAR(64) NOT NULL,
    merchandise_amount DECIMAL(8,2),
    tax_amount DECIMAL(8,2),
    payment_type VARCHAR(32),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (order_id)
);

ALTER TABLE sales_header ADD CONSTRAINT sales_header_fk1 FOREIGN KEY (channel_id) REFERENCES  channel(channel_id);
ALTER TABLE sales_header ADD CONSTRAINT sales_header_fk2 FOREIGN KEY (customer_id) REFERENCES  customer(customer_id);
ALTER TABLE sales_header ADD CONSTRAINT sales_header_fk3 FOREIGN KEY (primary_associate_id) REFERENCES  employee(employee_id);
ALTER TABLE sales_header ADD CONSTRAINT sales_header_fk4 FOREIGN KEY (secondary_associate_id) REFERENCES  employee(employee_id);
ALTER TABLE sales_header ADD CONSTRAINT sales_header_fk5 FOREIGN KEY (store_id) REFERENCES  store(store_id);

CREATE TABLE sales_line_item (
    line_item_id BIGINT NOT NULL AUTO_INCREMENT,
    order_id BIGINT NOT NULL,
    item_id BIGINT NOT NULL,
    transaction_type VARCHAR(32),
    quantity SMALLINT NOT NULL,
    item_price_each DECIMAL(8,2) NOT NULL,
    item_tax_each DECIMAL(8,2) NOT NULL,
    markdown_each DECIMAL(8,2) DEFAULT 0.0,
    freight_each DECIMAL(8,2) DEFAULT 0.0,
    duty_each DECIMAL(8,2) DEFAULT 0.0,
    gift_wrap_flag BOOLEAN DEFAULT FALSE,
    item_status VARCHAR(64) NOT NULL,
    item_status_as_of_date DATE NOT NULL,
    ship_date DATE,
    promotion_code VARCHAR(32),
    gift_message TEXT,
    gift_registry_number VARCHAR(64),
    created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (line_item_id)
);

ALTER TABLE sales_line_item ADD CONSTRAINT sales_line_item_fk1 FOREIGN KEY (order_id) REFERENCES  sales_header(order_id);
ALTER TABLE sales_line_item ADD CONSTRAINT sales_line_item_fk2 FOREIGN KEY (item_id) REFERENCES  item(item_id);
