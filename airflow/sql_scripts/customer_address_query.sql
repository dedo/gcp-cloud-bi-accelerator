SELECT address_id, customer_id, address_line_1, address_line_2, city, state, postal_code, country, created_timestamp, updated_timestamp
FROM ulta_retail_demo_data.customer_address;