SELECT
  cust_pref.customer_id AS customer_id,
  customer_number,
  last_name,
  first_name,
  alternate_lastName,
  salutation,
  birth_date,
  gender,
  marital_status,
  anniversary,
  primary_email,
  secondary_email,
  primary_phone,
  primary_phone_type,
  secondary_phone,
  secondary_phone_type,
  employee_flag,
  social_handle_1,
  social_handle_1_type,
  social_handle_2,
  social_handle_2_type,
  household_id,
  preference_id,
  preference_type,
  preference_value,
  loyal.customer_loyalty_id AS loyalty_id,
  loyal.loyalty_number AS loyalty_number,
  loyal.loyalty_join_date AS loyalty_join_date,
  loyal.loyalty_lifetime_points AS loyalty_lifetime_points,
  loyal.loyalty_available_points AS loyalty_available_points,
  loyal.loyalty_level AS loyalty_level,
  loyal.loyalty_level_description AS loyalty_level_description,
  loyal.loyalty_level_expiration AS loyalty_level_expiration
FROM (
  SELECT
    cust.customer_id AS customer_id,
    customer_number,
    last_name,
    first_name,
    alternate_lastName,
    salutation,
    birth_date,
    gender,
    marital_status,
    anniversary,
    primary_email,
    secondary_email,
    primary_phone,
    primary_phone_type,
    secondary_phone,
    secondary_phone_type,
    employee_flag,
    social_handle_1,
    social_handle_1_type,
    social_handle_2,
    social_handle_2_type,
    household_id,
    pref.customer_preference_id AS preference_id,
    pref.customer_preference_type AS preference_type,
    pref.customer_preference_value AS preference_value
  FROM (
    SELECT
      *
    FROM
      [ziptie-ulta-demo:retail_demo_warehouse.customer] ) cust
  LEFT OUTER JOIN (
    SELECT
      customer_id,
      customer_preference_id,
      customer_preference_type,
      customer_preference_value
    FROM
      [ziptie-ulta-demo:retail_demo_warehouse.customer_preferences] ) pref
  ON
    cust.customer_id = pref.customer_id) cust_pref
LEFT OUTER JOIN (
  SELECT
    customer_id,
    customer_loyalty_id,
    loyalty_number,
    loyalty_join_date,
    loyalty_lifetime_points,
    loyalty_available_points,
    loyalty_level,
    loyalty_level_description,
    loyalty_level_expiration
  FROM
    [ziptie-ulta-demo:retail_demo_warehouse.customer_loyalty] ) loyal
ON
  cust_pref.customer_id = loyal.customer_id