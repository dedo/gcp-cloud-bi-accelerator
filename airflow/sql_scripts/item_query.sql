SELECT item_id, vendor_id, division_id, department_id, classification_id, sub_classification_id, style_id, sku, color, `size`, short_description, replace(replace(replace(long_description, '\n', ''), '\r', ''), ',', '&#44;') as long_description, image_url, current_price, created_timestamp, updated_timestamp
FROM ulta_retail_demo_data.item
WHERE updated_timestamp > '{last_timestamp}'