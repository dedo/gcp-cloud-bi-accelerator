import pytest
from gcs_handler import delete_gcs_file
from gcs_handler import delete_gcs_folder
from gcs_handler import list_gcs_folder
from contextlib import contextmanager


@pytest.mark.parametrize("bucket_name, folder_name, expected_exception", [
    ('ulta-datalake', 'staged/channel', None),
])
def test_delete_gcs_folder(bucket_name, folder_name, expected_exception):
    with raises(expected_exception):
        delete_gcs_folder(bucket_name, folder_name)


@pytest.mark.parametrize("bucket_name, file_name, expected_exception", [
    ('ulta-datalake', 'staged/channel/channel.2018-05-18-08-44-42.csv', None),
])
def test_delete_gcs_file(bucket_name, file_name, expected_exception):
    with raises(expected_exception):
        delete_gcs_file(bucket_name, file_name)


@pytest.mark.parametrize("folder_name, bucket_name, expected_exception", [
    ('staged/customer/', 'ulta-datalake', None),
])
def test_list_gcs_folder(folder_name, bucket_name, expected_exception):
    with raises(expected_exception):
        directory_list = list_gcs_folder(folder_name, bucket_name)
        for entry in directory_list:
            print(entry)


def raises(error):
    """
    Wrapper around pytest.raises to support None.
    """
    if error:
        return pytest.raises(error)
    else:
        @contextmanager
        def not_raises():
            try:
                yield
            except Exception as e:
                raise e

        return not_raises()
