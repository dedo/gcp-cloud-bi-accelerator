import datetime
import math
from contextlib import contextmanager

import pytest

from db_source_to_gcs import get_sql_query_from_file
from db_source_to_gcs import get_sql_query_from_file_with_last_run_timestamp
from db_source_to_gcs import open_mysql_cnx
from db_source_to_gcs import pack_lawson_date_time_to_timestamp
from db_source_to_gcs import read_db_to_gcs
from db_source_to_gcs import unpack_timestamp_to_lawson_date_time
from db_source_to_gcs import write_last_run_timestamp


@pytest.mark.parametrize("cnx_name, expected_exception", [
    ('ulta_cloud_mysql', None),
])
def test_get_good_mysql_cnx(cnx_name, expected_exception):
    with raises(expected_exception):
        open_mysql_cnx(cnx_name)


@pytest.mark.parametrize(
    "cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name, staging_folder_name, file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception",
    [
        # ('ulta_cloud_mysql', 'channel', '/Users/cdebracy/dev/Ulta/airflow/sql_scripts/channel_query.sql',
        #   'ulta-datalake', 'raw/channel', 'staged/channel', 'channel.', False, False,
        #   "ulta_cloud_mysql",
        #   None),
        ('ulta_cloud_mysql', 'item', '/Users/cdebracy/dev/Ulta/airflow/sql_scripts/item_query.sql',
         'ulta-datalake', 'raw/item', 'staged/item', 'item.', True, False,
         "ulta_cloud_mysql",
         None),
    ])
def test_read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                        staging_folder_name,
                        file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception):
    """
    Tests the core code for the airflow AbraSourceToS3Operator
    :param cnx_name: Connection name to retrieve the password and db connection info.
    :param source_data_dates_name: Name of the record for SOURCE_DATA_DATES when performing incremental loading, None otherwise.
    :param script_name: Name of the script to read data.
    :param bucket_name: Name of the S3 bucket to store results of query in CSV format.
    :param data_lake_folder_name: Name of the folder to store the file in.
    :param staging_folder_name: Name of the staging folder for the S3 file.
    :param file_prefix: Filename prefix to use. The timestamp is appended in the code.
    :param uses_timestamp: Boolean to indicate that a timestamp is tracked for this data source for incremental loads.
    :param pack_timestamp: Boolean to indicate whether the timestamp is stored in separate date and number columns as used by Lawson
    :param snowflake_cnx_name: Connection name for snowflake user. May be None when no last run timestamp is maintained.
    :return:
    """
    with raises(expected_exception):
        read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                       staging_folder_name,
                       file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name)


def test_get_bad_sql_query_from_file():
    """Returns an SQL query string stored in the file passed in.
    """
    with pytest.raises(FileNotFoundError):
        get_sql_query_from_file("/C/nofile")


@pytest.mark.parametrize("file_name", [
    "/Users/cdebracy/dev/ABRA/repo/abra-airflow/sql_scripts/glamounts_query.sql",
    "/Users/cdebracy/dev/ABRA/repo/abra-airflow/sql_scripts/store_list_query.sql",
])
def test_get_good_sql_query_from_file(file_name):
    """Returns an SQL query string stored in the file passed in.
    :param filename: File name/path that contains the SQL query to execute.
    """
    sql = get_sql_query_from_file(file_name)
    assert len(sql) > 0


@pytest.mark.parametrize("maint_date, maint_time", [
    ("2018-01-02", 10319),
    ("2017-01-02", 171531),
])
def test_convert_lawson_date_time_to_datetime(maint_date, maint_time):
    converted_date = datetime.datetime.strptime(maint_date, "%Y-%m-%d").date()

    seconds = maint_time / 3
    hours = seconds // 3600
    minutes = seconds // 60 - hours * 60
    converted_time = datetime.time(hour=int(math.floor(hours)), minute=int(math.floor(minutes)))
    print(converted_date)
    print(converted_time)
    timestamp = datetime.datetime.combine(converted_date, converted_time)
    print(timestamp)


@pytest.mark.parametrize("date_to_pack, time_to_pack, expected", [
    (datetime.datetime(2018, 1, 2, 0, 0, 0), 10319, None),
    (datetime.datetime(2017, 1, 2, 0, 0, 0), 15342, None),
    ("2018-01-25", 10319, TypeError),
    (datetime.datetime(2017, 1, 2, 0, 0, 0), "14:20", TypeError),
])
def test_pack_lawson_date_time_to_timestamp(date_to_pack, time_to_pack, expected):
    with raises(expected):
        pack_lawson_date_time_to_timestamp(date_to_pack, time_to_pack)


@pytest.mark.parametrize("timestamp_to_unpack, expected", [
    (datetime.datetime(2018, 1, 2, 15, 23, 10, 182703), None),
])
def test_unpack_timestamp_to_lawson_date_time(timestamp_to_unpack, expected):
    with raises(expected):
        maint_date, maint_time = unpack_timestamp_to_lawson_date_time(timestamp_to_unpack)
        assert isinstance(maint_date, datetime.date)
        assert isinstance(maint_time, int)


@pytest.mark.parametrize(
    "snowflake_cnx_name, source_data_dates_name, filename, pack_timestamp, dttm, expected_exception", [
        ("snowflake-etl", "source_data_dates_name",
         "/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/glamounts_query.sql", True,
         datetime.datetime.now(),
         None),
    ])
def test_get_sql_query_from_file_with_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, filename,
                                                         pack_timestamp,
                                                         dttm,
                                                         expected_exception):
    with raises(expected_exception):
        sql = get_sql_query_from_file_with_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, filename,
                                                              pack_timestamp,
                                                              dttm)
        assert len(sql) > 1
        print(sql)


@pytest.mark.parametrize("snowflake_cnx_name, source_data_dates_name, timestamp, expected_exception", [

    ("snowflake-etl", "source_data_dates", datetime.datetime.now(), None),

])
def test_write_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, timestamp, expected_exception):
    """
    Because this writes data to the database, RUN THIS AGAINST DEV ONLY!
    :param snowflake_cnx_name: Snowflake pwd file to decrypt.
    :param source_data_dates_name: Data source record to update in SOURCE_DATA_DATES
    :param timestamp: Timestamp to update.
    :param expected_exception: Exception expected to be raised, or None.
    """
    with raises(expected_exception):
        write_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, timestamp)


@pytest.mark.parametrize(
    "cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name, staging_folder_name, file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception",
    [
        # ('glamounts-awsuser', 'glamounts',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/glamounts_query.sql',
        #  'dev-abra-finance-datalake', 'Lawson/GLAmounts/raw', None, 'GLAmounts.csv.', True, True,
        #  "snowflake-etl",
        #  None),
        ('gltrans-awsuser', 'gltrans', '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/gltrans_query.sql',
         'dev-abra-finance-datalake', 'Lawson/GLTrans/raw', None, 'GLTrans.csv.', True, True,
         "snowflake-etl",
         None),
        # ('store_list-awsuser', 'store_list',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/store_list_query.sql',
        #  'dev-abra-finance-datalake', 'Lawson/StoreList/raw', None, 'StoreList.csv.', False, False, None, None),
        # ('account_mapping-awsuser', 'account_mapping',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/sql_scripts/account_mapping_query.sql',
        #  'dev-abra-finance-datalake', 'Lawson/AccountMapping/raw', 'Lawson/AccountMapping/staged',
        #  'AccountMapping.csv.', False, False, None,
        #  None),
        # ('kronos-awsuser', 'kronos', '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/kronos_query.sql',
        #  'dev-abra-hr-datalake', 'Kronos/raw', None, 'Kronos.csv.', True, False, "snowflake-etl", None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/categories_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Categories/raw', 'ABSE/Categories/staged', 'Categories.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/employee_labor_rates_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/EmployeeLaborRates/raw', 'ABSE/EmployeeLaborRates/staged',
        #  'EmployeeLaborRates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/employees_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Employees/raw', 'ABSE/Employees/staged',
        #  'Employees.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_details_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/EstimatesDetails/raw', 'ABSE/EstimatesDetails/staged',
        #  'EstimatesDetails.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_labor_rates_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/EstimatesLaborRates/raw', 'ABSE/EstimatesLaborRates/staged',
        #  'EstimatesLaborRates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Estimates/raw', 'ABSE/Estimates/staged',
        #  'Estimates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_cost_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/JobCost/raw', 'ABSE/JobCost/staged',
        #  'JobCost.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_credits_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/JobCredits/raw', 'ABSE/JobCredits/staged',
        #  'JobCredits.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_invoices_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/JobInvoices/raw', 'ABSE/JobInvoices/staged',
        #  'JobInvoices.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/jobs_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Jobs/raw', 'ABSE/Jobs/staged',
        #  'Jobs.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/owner_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Owner/raw', 'ABSE/Owner/staged',
        #  'Owner.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', None,
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/sublet_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/Sublet/raw', 'ABSE/Sublet/staged',
        #  'Sublet.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', "ops_overview_nrt",
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/ops_overview_nrt_query.sql',
        #  'dev-abra-operations-datalake', 'ABSE/OpsOverview_NRT/raw', 'ABSE/OpsOverview_NRT/staged',
        #  'OpsOverview_NRT.csv.', True,
        #  False, 'snowflake-etl',
        #  None),
    ])
def test_dev_read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                            staging_folder_name,
                            file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception):
    """
    Tests the core code for the airflow AbraSourceToS3Operator
    :param cnx_name: Connection name to retrieve the password and db connection info.
    :param source_data_dates_name: Name of the record for SOURCE_DATA_DATES when performing incremental loading, None otherwise.
    :param script_name: Name of the script to read data.
    :param bucket_name: Name of the S3 bucket to store results of query in CSV format.
    :param data_lake_folder_name: Name of the folder to store the file in.
    :param staging_folder_name: Name of the staging folder for the S3 file.
    :param file_prefix: Filename prefix to use. The timestamp is appended in the code.
    :param uses_timestamp: Boolean to indicate that a timestamp is tracked for this data source for incremental loads.
    :param pack_timestamp: Boolean to indicate whether the timestamp is stored in separate date and number columns as used by Lawson
    :param snowflake_cnx_name: Connection name for snowflake user. May be None when no last run timestamp is maintained.
    :return:
    """
    with raises(expected_exception):
        read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                       staging_folder_name,
                       file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name)


@pytest.mark.parametrize(
    "cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name, staging_folder_name, file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception",
    [
        # ('glamounts-awsuser', 'glamounts',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/glamounts_query.sql',
        #  'abra-finance-datalake', 'Lawson/GLAmounts/raw', None, 'GLAmounts.csv.', True, True,
        #  "snowflake-etl",
        #  None),
        # ('gltrans-awsuser', 'gltrans', '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/gltrans_query.sql',
        #  'abra-finance-datalake', 'Lawson/GLTrans/raw', None, 'GLTrans.csv.', True, True,
        #  "source_data_dates-etl_dev",
        #  None),
        # ('store_list-awsuser', 'store_list',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/store_list_query.sql',
        #  'abra-finance-datalake', 'Lawson/StoreList/raw', None, 'StoreList.csv.', False, False, None, None),
        # ('account_mapping-awsuser', 'account_mapping',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/sql_scripts/account_mapping_query.sql',
        #  'abra-finance-datalake', 'Lawson/AccountMapping/raw', 'Lawson/AccountMapping/staged',
        #  'AccountMapping.csv.', False, False, None,
        #  None),
        # ('kronos-awsuser', 'kronos', '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/kronos_query.sql',
        #  'dev-abra-hr-datalake', 'Kronos/raw', None, 'Kronos.csv.', True, False, "source_data_dates-etl_dev", None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/categories_query.sql',
        #  'abra-operations-datalake', 'ABSE/Categories/raw', 'ABSE/Categories/staged', 'Categories.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/employee_labor_rates_query.sql',
        #  'abra-operations-datalake', 'ABSE/EmployeeLaborRates/raw', 'ABSE/EmployeeLaborRates/staged',
        #  'EmployeeLaborRates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/employees_query.sql',
        #  'abra-operations-datalake', 'ABSE/Employees/raw', 'ABSE/Employees/staged',
        #  'Employees.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_details_query.sql',
        #  'abra-operations-datalake', 'ABSE/EstimatesDetails/raw', 'ABSE/EstimatesDetails/staged',
        #  'EstimatesDetails.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_labor_rates_query.sql',
        #  'abra-operations-datalake', 'ABSE/EstimatesLaborRates/raw', 'ABSE/EstimatesLaborRates/staged',
        #  'EstimatesLaborRates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/estimates_query.sql',
        #  'abra-operations-datalake', 'ABSE/Estimates/raw', 'ABSE/Estimates/staged',
        #  'Estimates.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_cost_query.sql',
        #  'abra-operations-datalake', 'ABSE/JobCost/raw', 'ABSE/JobCost/staged',
        #  'JobCost.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_credits_query.sql',
        #  'abra-operations-datalake', 'ABSE/JobCredits/raw', 'ABSE/JobCredits/staged',
        #  'JobCredits.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/job_invoices_query.sql',
        #  'abra-operations-datalake', 'ABSE/JobInvoices/raw', 'ABSE/JobInvoices/staged',
        #  'JobInvoices.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/jobs_query.sql',
        #  'abra-operations-datalake', 'ABSE/Jobs/raw', 'ABSE/Jobs/staged',
        #  'Jobs.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/owner_query.sql',
        #  'abra-operations-datalake', 'ABSE/Owner/raw', 'ABSE/Owner/staged',
        #  'Owner.csv.', False,
        #  False, None,
        #  None),
        # ('abse-awsuser', 'abse',
        #  '/Users/cdebracy/dev/ABRA/repo/abra-airflow/tests/sql_scripts/sublet_query.sql',
        #  'abra-operations-datalake', 'ABSE/Sublet/raw', 'ABSE/Sublet/staged',
        #  'Sublet.csv.', False,
        #  False, None,
        #  None),
    ])
def test_prod_read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                             staging_folder_name,
                             file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name, expected_exception):
    """
    Tests the core code for the airflow AbraSourceToS3Operator
    :param cnx_name: Connection name to retrieve the password and db connection info.
    :param source_data_dates_name: Name of the SOURCE_DATA_DATES record to use for incremental loads, None otherwise.
    :param script_name: Name of the script to read data.
    :param bucket_name: Name of the S3 bucket to store results of query in CSV format.
    :param data_lake_folder_name: Name of the folder to store the file in.
    :param staging_folder_name: Name of the staging folder for the S3 file.
    :param file_prefix: Filename prefix to use. The timestamp is appended in the code.
    :param uses_timestamp: Boolean to indicate that a timestamp is tracked for this data source for incremental loads.
    :param pack_timestamp: Boolean to indicate whether the timestamp is stored in separate date and number columns as used by Lawson
    :param snowflake_cnx_name: Connection name for snowflake user. May be None when no last run timestamp is maintained.
    :return:
    """
    with raises(expected_exception):
        read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                       staging_folder_name,
                       file_prefix, uses_timestamp, pack_timestamp, snowflake_cnx_name)


def raises(error):
    """
    Wrapper around pytest.raises to support None.
    """
    if error:
        return pytest.raises(error)
    else:
        @contextmanager
        def not_raises():
            try:
                yield
            except Exception as e:
                raise e

        return not_raises()
