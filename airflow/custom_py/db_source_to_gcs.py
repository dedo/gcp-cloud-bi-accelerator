import csv
import datetime
import logging as log
import math
from io import StringIO

import pandas.io.sql as psql
from airflow.hooks.mysql_hook import MySqlHook

from gcs_handler import copy_gcs_file
from gcs_handler import write_to_gcs_bucket

__DATA_SOURCE_SQL__ = "SELECT * FROM ulta_retail_demo_data.SOURCE_DATA_DATES WHERE DATA_SOURCE = '%s'"


def open_mysql_cnx(cnx_name):
    """
    Opens a SQL Server connection object. The source name is needed because multiple databases are accessed.
    :param cnx_name: Connection name to decrypt.
    :return: A SQL Server connection.
    """
    try:
        mysql = MySqlHook(mysql_conn_id=cnx_name)
        conn = mysql.get_conn()
    except Exception as e:
        log.error('Error: %s', e.msg)
    return conn


def read_db_to_gcs(cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                   staging_folder_name,
                   file_prefix, uses_timestamp,
                   pack_timestamp, snowflake_cnx_name):
    """
    Main function called by AbraSourceToS3Operator.
    :param cnx_name: Connection name. This will match the name on AWS that contains the username/password for the database.
    :param source_data_dates_name: The record name in SOURCE_DATA_DATES for incremental loads. Will be set to None otherwise.
    :param script_name: The SQL script to run that queries the data.
    :param bucket_name: The S3 bucket to write to.
    :param data_lake_folder_name: The data lake folder in the S3 bucket to write to.
    :param staging_folder_name: Optional folder to copy the file to for Snowflake staging.
    :param file_prefix: The prefix of the filename, prior to adding the YYYY-MM-DD-HH-MM-SS extension.
    :param uses_timestamp: Boolean to indicate whether the data source has a timestamp field we're persisting for incremental runs.
    :param pack_timestamp: Boolean to indicate whether the date and time are stored in 2 separate columns ala Lawson.
    :param snowflake_cnx_name: Name of the snowflake connection information to decrypt on S3. May be None if there's no need to track the last run for that data source.
    """
    print(bucket_name)
    conn = open_mysql_cnx(cnx_name)
    if conn is None:
        raise MySqlHook.OperationalError(
            "No connection could be established from the information found in cnx_name: {0}".format(cnx_name))

    # Some data sources don't have a timestamp for incremental queries.
    dt = datetime.datetime.now()
    formatted_dt = dt.strftime("%Y-%m-%d-%H-%M-%S")
    try:
        if uses_timestamp is False:
            sql = get_sql_query_from_file(script_name)
            sql = sql.format(effective_dttm=dt)
        else:
            sql = get_sql_query_from_file_with_last_run_timestamp(snowflake_cnx_name, source_data_dates_name,
                                                                  script_name,
                                                                  pack_timestamp, dt)

        df = psql.read_sql_query(sql, conn)
        csv_buffer = StringIO()

        # We're done if we didn't pull in any data
        if len(df.index) == 0:
            log.info("No records found for this run.")
            return 0
        else:
            log.info("Processing %d rows in this run.", len(df.index))

        df.to_csv(csv_buffer, encoding='utf-8', index=False, quoting=csv.QUOTE_ALL, header=False)
        write_to_gcs_bucket(bucket_name, data_lake_folder_name, file_prefix + formatted_dt + '.csv', csv_buffer)

        if staging_folder_name is not None:
            source_filename = data_lake_folder_name + '/' + file_prefix + formatted_dt + '.csv'
            destination_filename = staging_folder_name + '/' + file_prefix + formatted_dt + '.csv'
            copy_gcs_file(bucket_name, source_filename, bucket_name, destination_filename)

        # Update the timestamp in the SOURCE_DATA_DATES to handle the next run. Use the datetime from before the s3
        # bucket write, but only update it if records were processed.
        if len(df.index) > 1 and uses_timestamp is True:
            write_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, dt)

    except MySqlHook.OperationalError as oe:
        log.error('Error: %s', oe.msg)

    except FileNotFoundError as fnf_error:
        log.error("File not found! %s\n%s", script_name, fnf_error.strerror)

    finally:
        csv_buffer.close()
        conn.close()
        log.info("End of abra_source_to_s3_operator.")


def get_sql_query_from_file(filename):
    """Returns an SQL query string stored in the file passed in.
    :param filename: File name/path that contains the SQL query to execute.
    """
    with open(filename, "r") as file:
        try:
            sql_query = file.read()
            log.debug(sql_query)
        except FileNotFoundError as fnf_error:
            log.error("File not found: %s, %s", filename, fnf_error)
            raise FileNotFoundError
        except Exception as error:
            log.error("Unknown error: ", error)

        if len(sql_query) > 0:
            return sql_query
        else:
            return None


def pack_lawson_date_time_to_timestamp(date_to_pack, time_to_pack):
    """
    Converts the Lawson MAINT_DATE and MAINT_TIME columns into a single timestamp. In SQL Server
    the time is an integer that represents the # of seconds since midnight. Every 3 ticks represents one second.
    :param date_to_pack: Date from Lawson
    :param time_to_pack: Time from Lawson. Every 3 ticks is one second since midnight.
    :return: timestamp with the combined values
    """
    try:
        seconds = time_to_pack / 3
        hours = seconds // 3600
        minutes = seconds // 60 - hours * 60
        converted_time = datetime.time(hour=int(math.floor(hours)), minute=int(math.floor(minutes)))
        timestamp = datetime.datetime.combine(date_to_pack, converted_time)
        return timestamp
    except ValueError as ve:
        log.error("Value Error: %s", ve)
        return None


def unpack_timestamp_to_lawson_date_time(timestamp):
    """
    Unpacks a datetime.datetime timestamp into separate date and integer values for Lawson's use
    :param timestamp: Timestamp to unpack
    :return: Returns s
    """
    log.debug(timestamp)
    lawson_date = datetime.datetime.strptime(str(timestamp), "%Y-%m-%d %H:%M:%S.%f").date()
    log.debug(lawson_date)

    lawson_time = datetime.datetime.strptime(str(timestamp), "%Y-%m-%d %H:%M:%S.%f").time()
    log.debug(type(lawson_time))

    split_time = str(lawson_time).split(":")

    hours = int(split_time[0])
    log.debug(hours)
    minutes = int(split_time[1])
    log.debug(minutes)
    seconds = int(math.floor(float(split_time[2])))
    log.debug(seconds)

    minutes += (hours * 60)
    lawson_seconds = ((minutes * 60) + seconds) * 3
    log.debug(lawson_seconds)

    return lawson_date, lawson_seconds


def get_sql_query_from_file_with_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, filename,
                                                    pack_timestamp, dttm):
    """
    Retrieves an SQL query file and updates the string with the appropriate timestamps for incremental loading.
    :param snowflake_cnx_name: Snowflake connection name for this environment.
    :param source_data_dates_name: The record to find in SOURCE_DATA_DATES.
    :param filename: The SQL query file to load.
    :param pack_timestamp: Boolean to indicate whether the timestamp is packed like Lawson uses.
    :param dttm: Current datetime stamp.
    :return: Returns the updated SQL query as a string.
    """
    global __DATA_SOURCE_SQL__
    data_source = source_data_dates_name.upper()

    conn = open_mysql_cnx(snowflake_cnx_name)
    try:
        sql = __DATA_SOURCE_SQL__ % data_source
        log.debug(sql)

        # pull the last run out of the database for this data source
        df = psql.read_sql(sql, conn)

        # if the record doesn't exist, default to a timestamp 1/1/70 00:00:001.
        if len(df.index) == 0:
            timestamp = datetime.datetime(1970, 1, 1, 0, 0, 1, 1)
        else:
            timestamp = df.loc[(0, 'LAST_RUN_TIMESTAMP')]

        # Remove the least 3 significant milliseconds for SQL Server's timestamp
        timestamp_str = timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')
        timestamp_str = timestamp_str[:-3]

        log.debug(timestamp)
        sql_query = get_sql_query_from_file(filename)

        # If the data_source specifies the timestamp is packed, then unpack the timestamp
        if pack_timestamp is True:
            last_run_date, last_run_time = unpack_timestamp_to_lawson_date_time(timestamp)
            sql_query = sql_query.format(effective_dttm=dttm, last_run_date=last_run_date,
                                         last_run_time=last_run_time)
        else:
            sql_query = sql_query.format(effective_dttm=dttm, last_timestamp=timestamp_str)

        log.debug(sql_query)
        return sql_query

    except Exception as ee:
        log.error("Error {0}".format(ee.msg))

    finally:
        conn.close()


def write_last_run_timestamp(snowflake_cnx_name, source_data_dates_name, timestamp):
    """
    Retrieves and updates the last run timestamp. If no record exists, it creates one.
    :param snowflake_cnx_name: The snowflake connection name for SOURCE_DATA_DATEs
    :param source_data_dates_name: The record name we're updating
    :param timestamp: The current timestamp to store. The timestamp is stored without a TZ
    """
    # Make this uppercase for consistency in retrieving the data from the table
    data_source = source_data_dates_name.upper()

    conn = open_mysql_cnx(snowflake_cnx_name)
    try:
        sql = __DATA_SOURCE_SQL__ % data_source
        cursor = conn.cursor()
        cursor.execute(sql)
        date = timestamp.date()
        time = timestamp.time()
        if cursor.rowcount == 0:
            sql_insert = "INSERT INTO ulta_retail_demo_data.SOURCE_DATA_DATES \
                VALUES ('{data_source}', TIMESTAMP('{date}', '{time}'))".format(data_source=data_source, date=date,
                                                                                time=time)
            cursor.execute(sql_insert)
        else:
            sql_update = "UPDATE ulta_retail_demo_data.SOURCE_DATA_DATES SET LAST_RUN_TIMESTAMP = TIMESTAMP('{date}', '{time}') \
                WHERE ulta_retail_demo_data.SOURCE_DATA_DATES.DATA_SOURCE = '{data_source}'".format(
                data_source=data_source, date=date, time=time)
            cursor.execute(sql_update)

        conn.commit()
        log.info('Updating {data_source} in SOURCE_DATA_DATES with a new timestamp of {date}, {time}'.format(
            data_source=data_source, date=date, time=time))

    except Exception as e:
        log.error('Error {0}'.format(e))

    finally:
        conn.close()
