#!/bin/bash

echo "Copying the last run date for sales_fact.."
gsutil cp gs://ulta-keys/sales-fact-rundate.txt /tmp/
echo "Copying the sales_fact SQL.."
gsutil cp gs://ulta-dataflow/sales-fact-bigquery/sales_fact_join_query.sql /tmp/
START_TS=`cat /tmp/sales-fact-rundate.txt`
echo "START_TS: $START_TS"
PARAM_STR="start_ts:TIMESTAMP:$START_TS"
echo "PARAM_STR: $PARAM_STR"
SQL_FILE="/tmp/sales_fact_join_query.sql"
echo "SQL_FILE: $SQL_FILE"
echo "Executing the BQ query..."
bq query --nouse_legacy_sql --parameter="`echo $PARAM_STR`" `cat $SQL_FILE`
echo "Finished executing the sales_fact BQ query..."
START_DT=$(date -d "$START_TS 5 minutes" '+%Y-%m-%d %H:%M:%S')
echo "START_DT: $START_DT, writing it to file"
echo $START_DT > /tmp/sales-fact-rundate.txt
echo "Copying the updated rundate file to GCS.."
gsutil cp /tmp/sales-fact-rundate.txt gs://ulta-keys/
