from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

from db_source_to_gcs import read_db_to_gcs


class DBSourceToGCSOperator(BaseOperator):

    @apply_defaults
    def __init__(self, cnx_name, source_data_dates_name, script_name, bucket_name, data_lake_folder_name,
                 staging_folder_name,
                 file_prefix, uses_timestamp,
                 pack_timestamp, snowflake_cnx_name, *args, **kwargs):
        super(DBSourceToGCSOperator, self).__init__(*args, **kwargs)
        self.source_data_dates_name = source_data_dates_name
        self.script_name = script_name
        self.bucket_name = bucket_name
        self.data_lake_folder_name = data_lake_folder_name
        self.staging_folder_name = staging_folder_name
        self.file_prefix = file_prefix
        self.uses_timestamp = uses_timestamp
        self.pack_timestamp = pack_timestamp
        self.snowflake_cnx_name = snowflake_cnx_name
        self.cnx_name = cnx_name

    def execute(self, context):
        return read_db_to_gcs(self.cnx_name, self.source_data_dates_name, self.script_name, self.bucket_name,
                              self.data_lake_folder_name,
                              self.staging_folder_name, self.file_prefix, self.uses_timestamp, self.pack_timestamp,
                              self.snowflake_cnx_name)


class DBSourceToGCSPlugin(AirflowPlugin):
    name = 'db_source_to_gcs_plugin'
    operators = [DBSourceToGCSOperator]
    hooks = []
    executors = []
    macros = []
    admin_views = []
    flask_blueprints = []
    menu_links = []
