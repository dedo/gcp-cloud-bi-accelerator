import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator

from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('customer_processing',
          schedule_interval='@monthly',
          catchup=False,
          default_args=default_args)

customer_gcs_folder_delete_task = GCSFolderDeleteOperator(task_id='customer_staged_folder_delete',
                                                          bucket_name='ulta-datalake',
                                                          folder_name='staged/customer',
                                                          dag=dag)

customer_read_task = DBSourceToGCSOperator(task_id='customer_read', cnx_name='ulta_cloud_mysql',
                                           source_data_dates_name='customer',
                                           script_name=airflow_home + '/sql_scripts/customer_query.sql',
                                           bucket_name='ulta-datalake',
                                           data_lake_folder_name='raw/customer',
                                           staging_folder_name='staged/customer',
                                           file_prefix='customer.', uses_timestamp=False,
                                           pack_timestamp=False, snowflake_cnx_name='ulta_cloud_mysql',
                                           trigger_rule='all_done', dag=dag)

customer_gcs_folder_delete_task.set_downstream(customer_read_task)

# Add the terminating forward slash to stick to the single folder in the bucket
file_list = list_gcs_folder('staged/customer/', 'ulta-datalake')
for file_name in file_list:
    customer_df_task = BashOperator(task_id='customer_df.' + file_name,
                                    bash_command='gcloud dataflow jobs run CustomerLoad-{customer_file_name} \
--gcs-location=gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json \
--zone=us-central1-c \
--parameters javascriptTextTransformFunctionName=transform,JSONPath=gs://ulta-dataflow/gcs-to-bigquery/schemas/customer.json,javascriptTextTransformGcsPath=gs://ulta-dataflow/gcs-to-bigquery/udfs/customer.js,inputFilePattern=gs://ulta-datalake/staged/customer/{customer_file_name},outputTable=ziptie-ulta-demo:retail_demo_warehouse.customer,bigQueryLoadingTemporaryDirectory=gs://ulta-dataflow/gcs-to-bigquery/tmp'.format(
                                        customer_file_name=file_name),
                                    dag=dag)

    customer_read_task.set_downstream(customer_df_task)
