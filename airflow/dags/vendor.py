import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator

from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'dataflow_default_options': {
        'project': 'ziptie-ulta-demo',
        'zone': 'us-central1-c',
        'stagingLocation': 'gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json'
    }
}

dag = DAG('vendor_processing',
          schedule_interval='@monthly',
          catchup=False,
          default_args=default_args)

vendor_gcs_folder_delete_task = GCSFolderDeleteOperator(task_id='vendor_staged_folder_delete',
                                                        bucket_name='ulta-datalake',
                                                        folder_name='staged/vendor',
                                                        dag=dag)

vendor_read_task = DBSourceToGCSOperator(task_id='vendor_read', cnx_name='ulta_cloud_mysql',
                                         source_data_dates_name='vendor',
                                         script_name=airflow_home + '/sql_scripts/vendor_query.sql',
                                         bucket_name='ulta-datalake',
                                         data_lake_folder_name='raw/vendor',
                                         staging_folder_name='staged/vendor',
                                         file_prefix='vendor.', uses_timestamp=False,
                                         pack_timestamp=False, snowflake_cnx_name='ulta_cloud_mysql',
                                         trigger_rule='all_done', dag=dag)

vendor_gcs_folder_delete_task.set_downstream(vendor_read_task)

# Add the terminating forward slash to stick to the single folder in the bucket
file_list = list_gcs_folder('staged/vendor/', 'ulta-datalake')
for file_name in file_list:
    vendor_df_task = BashOperator(task_id='vendor_df.' + file_name,
                                  bash_command='gcloud dataflow jobs run VendorLoad-{vendor_file_name} \
--gcs-location=gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json \
--zone=us-central1-c \
--parameters javascriptTextTransformFunctionName=transform,JSONPath=gs://ulta-dataflow/gcs-to-bigquery/schemas/vendor.json,javascriptTextTransformGcsPath=gs://ulta-dataflow/gcs-to-bigquery/udfs/vendor.js,inputFilePattern=gs://ulta-datalake/staged/vendor/{vendor_file_name},outputTable=ziptie-ulta-demo:retail_demo_warehouse.vendor,bigQueryLoadingTemporaryDirectory=gs://ulta-dataflow/gcs-to-bigquery/tmp'.format(
                                      vendor_file_name=file_name),
                                  dag=dag)

    vendor_read_task.set_downstream(vendor_df_task)
