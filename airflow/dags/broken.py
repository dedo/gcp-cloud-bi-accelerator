import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator
from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('broken_channel_processing',
          schedule_interval='*/1 * * * *',
          catchup=False,
          default_args=default_args)

broken_channel_gcs_folder_delete_task = GCSFolderDeleteOperator(task_id='broken_channel_staged_folder_delete',
                                                                bucket_name='ulta-datalake',
                                                                folder_name='staged/broken_channel',
                                                                dag=dag)

broken_channel_read_task = DBSourceToGCSOperator(task_id='broken_channel_read', cnx_name='ulta_cloud_mysql',
                                                 source_data_dates_name='channel',
                                                 script_name=airflow_home + '/sql_scripts/channel_query.sql',
                                                 bucket_name='ulta-datalake',
                                                 data_lake_folder_name='raw/broken_channel',
                                                 staging_folder_name='staged/broken_channel',
                                                 file_prefix='channel.', uses_timestamp=False,
                                                 pack_timestamp=False, snowflake_cnx_name='ulta_cloud_mysql',
                                                 trigger_rule='all_done', dag=dag)

broken_channel_gcs_folder_delete_task.set_downstream(broken_channel_read_task)

# Add the terminating forward slash to stick to the single folder in the bucket
file_list = list_gcs_folder('staged/broken_channel/', 'ulta-datalake')
for file_name in file_list:
    broken_channel_df_task = BashOperator(task_id='broken_channel_df.' + file_name,
                                          bash_command='gcloud dataflow jobs run BrokenChannelLoad-{channel_file_name} \
    --gcs-location=gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json \
    --zone=us-central1-c \
    --parameters javascriptTextTransformFunctionName=transform,JSONPath=gs://ulta-dataflow/gcs-to-bigquery/schemas/channel.json,javascriptTextTransformGcsPath=gs://ulta-dataflow/gcs-to-bigquery/udfs/channel.js,inputFilePattern=gs://ulta-datalake/staged/channel_broken/{channel_file_name},outputTable=ziptie-ulta-demo:retail_demo_warehouse.channel,bigQueryLoadingTemporaryDirectory=gs://ulta-dataflow/gcs-to-bigquery/tmpn'.format(
                                              channel_file_name=file_name),
                                          dag=dag)

    broken_channel_read_task.set_downstream(broken_channel_df_task)
