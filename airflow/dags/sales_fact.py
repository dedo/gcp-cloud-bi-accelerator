import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

sales_run_fact_command = airflow_home + '/scripts/sales_fact_run.sh '

dag = DAG('sales_fact_processing',
          schedule_interval='*/5 * * * *',
          catchup=False,
          default_args=default_args)

sales_fact_task = BashOperator(task_id='sales_fact_streaming',
                               bash_command=sales_run_fact_command,
                               dag=dag)

