import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator

from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('customer_loyalty_processing',
          schedule_interval='@monthly',
          catchup=False,
          default_args=default_args)

customer_loyalty_gcs_folder_delete_task = GCSFolderDeleteOperator(task_id='customer_loyalty_staged_folder_delete',
                                                                  bucket_name='ulta-datalake',
                                                                  folder_name='staged/customer_loyalty',
                                                                  dag=dag)

customer_loyalty_read_task = DBSourceToGCSOperator(task_id='customer_loyalty_read', cnx_name='ulta_cloud_mysql',
                                                   source_data_dates_name='customer_loyalty',
                                                   script_name=airflow_home + '/sql_scripts/customer_loyalty_query.sql',
                                                   bucket_name='ulta-datalake',
                                                   data_lake_folder_name='raw/customer_loyalty',
                                                   staging_folder_name='staged/customer_loyalty',
                                                   file_prefix='customer_loyalty.', uses_timestamp=False,
                                                   pack_timestamp=False, snowflake_cnx_name='ulta_cloud_mysql',
                                                   trigger_rule='all_done', dag=dag)

customer_loyalty_gcs_folder_delete_task.set_downstream(customer_loyalty_read_task)

# Add the terminating forward slash to stick to the single folder in the bucket
file_list = list_gcs_folder('staged/customer_loyalty/', 'ulta-datalake')
for file_name in file_list:
    customer_loyalty_df_task = BashOperator(task_id='customer_loyalty_df.' + file_name,
                                            bash_command='gcloud dataflow jobs run CustomerLoyaltyLoad-{customer_loyalty_file_name} \
--gcs-location=gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json \
--zone=us-central1-c \
--parameters javascriptTextTransformFunctionName=transform,JSONPath=gs://ulta-dataflow/gcs-to-bigquery/schemas/customer-loyalty.json,javascriptTextTransformGcsPath=gs://ulta-dataflow/gcs-to-bigquery/udfs/customer-loyalty.js,inputFilePattern=gs://ulta-datalake/staged/customer_loyalty/{customer_loyalty_file_name},outputTable=ziptie-ulta-demo:retail_demo_warehouse.customer_loyalty,bigQueryLoadingTemporaryDirectory=gs://ulta-dataflow/gcs-to-bigquery/tmp'.format(
                                                customer_loyalty_file_name=file_name),
                                            dag=dag)

    customer_loyalty_read_task.set_downstream(customer_loyalty_df_task)
