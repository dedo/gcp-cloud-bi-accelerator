import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator

from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'dataflow_default_options': {
        'project': 'ziptie-ulta-demo',
        'zone': 'us-central1-c',
        'stagingLocation': 'gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json'
    }
}

dag = DAG('sub_classification_processing',
          schedule_interval='@monthly',
          catchup=False,
          default_args=default_args)

sub_classification_gcs_folder_delete_task = GCSFolderDeleteOperator(task_id='sub_classification_staged_folder_delete',
                                                                    bucket_name='ulta-datalake',
                                                                    folder_name='staged/sub_classification',
                                                                    dag=dag)

sub_classification_read_task = DBSourceToGCSOperator(task_id='sub_classification_read', cnx_name='ulta_cloud_mysql',
                                                     source_data_dates_name='sub_classification',
                                                     script_name=airflow_home + '/sql_scripts/sub_classification_query.sql',
                                                     bucket_name='ulta-datalake',
                                                     data_lake_folder_name='raw/sub_classification',
                                                     staging_folder_name='staged/sub_classification',
                                                     file_prefix='sub_classification.', uses_timestamp=False,
                                                     pack_timestamp=False, snowflake_cnx_name='ulta_cloud_mysql',
                                                     trigger_rule='all_done', dag=dag)

sub_classification_gcs_folder_delete_task.set_downstream(sub_classification_read_task)

# Add the terminating forward slash to stick to the single folder in the bucket
file_list = list_gcs_folder('staged/sub_classification/', 'ulta-datalake')
for file_name in file_list:
    sub_classification_df_task = BashOperator(task_id='sub_classification_df.' + file_name,
                                              bash_command='gcloud dataflow jobs run SubClassificationLoad-{sub_classification_file_name} \
--gcs-location=gs://ulta-dataflow/gcs-to-bigquery/templates/FileToBigQuery.json \
--zone=us-central1-c \
--parameters javascriptTextTransformFunctionName=transform,JSONPath=gs://ulta-dataflow/gcs-to-bigquery/schemas/sub-classification.json,javascriptTextTransformGcsPath=gs://ulta-dataflow/gcs-to-bigquery/udfs/sub-classification.js,inputFilePattern=gs://ulta-datalake/staged/sub_classification/{sub_classification_file_name},outputTable=ziptie-ulta-demo:retail_demo_warehouse.sub_classification,bigQueryLoadingTemporaryDirectory=gs://ulta-dataflow/gcs-to-bigquery/tmp'.format(
                                                  sub_classification_file_name=file_name),
                                              dag=dag)

    sub_classification_read_task.set_downstream(sub_classification_df_task)
