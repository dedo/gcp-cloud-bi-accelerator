import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.db_source_to_gcs_plugin import DBSourceToGCSOperator
from airflow.operators.gcs_folder_delete_plugin import GCSFolderDeleteOperator
from gcs_handler import list_gcs_folder

airflow_home = os.environ.get('AIRFLOW_HOME')

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 5, 18),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('sales_header_processing',
          schedule_interval='@once',
          catchup=False,
          default_args=default_args)

sales_header_streaming_task = BashOperator(task_id='sales_header_streaming',
                                           bash_command='gcloud dataflow jobs run SalesHeaderRawStreaming --gcs-location=gs://ulta-dataflow/pubsub-to-datastore/templates/Cloud_PubSub_to_GCS_Text --zone=us-central1-c --parameters inputTopic=projects/ziptie-ulta-demo/topics/sales-header,outputDirectory=gs://ulta-datalake/raw/sales_header/,outputFilenamePrefix=sales-header-,outputFilenameSuffix=.txt',
                                           dag=dag)

sales_header_raw_streaming_task = BashOperator(task_id='sales_header_raw_streaming',
                                               bash_command='gcloud dataflow jobs run SalesHeaderStreaming --gcs-location=gs://ulta-dataflow/pubsub-to-bigquery/templates/PubSubToBigQuery.json --zone=us-central1-c --parameters inputTopic=projects/ziptie-ulta-demo/topics/sales-header,outputTableSpec=ziptie-ulta-demo:retail_demo_warehouse.sales_header',
                                               dag=dag)

sales_header_streaming_task.set_downstream(sales_header_raw_streaming_task)
