from google.cloud import bigquery
from jinja2 import Template
import pandas
import pytest

BQ_ROW_COUNT_TEMPLATE = "SELECT COUNT(1) as record_count FROM `{{dataset}}.{{table_name}}`"

def validate_bq_table_row_count(suite_configuration, test_definition):
    bq_client = bigquery.Client(project=suite_configuration.gcp_project)
    query_render_dict = {}
    query_render_dict['dataset'] = test_definition.bq_dataset
    query_render_dict['table_name'] = test_definition.bq_table_name
    query_template = Template(BQ_ROW_COUNT_TEMPLATE)
    result_df = bq_client.query(query_template.render(query_render_dict)).to_dataframe()
    assert result_df.iloc[0]['record_count'] == list(test_definition.expected_results)[0]['result_value']

def validate_bq_custom_query(suite_configuration, test_definition):
    bq_client = bigquery.Client(project=suite_configuration.gcp_project)
    result_df = bq_client.query(test_definition.bq_query).to_dataframe()
    for expected_result in test_definition.expected_results:
        if expected_result['result_type'] == "decimal":
            expected_value = round(result_df.iloc[0][expected_result['result_name']],2)
        else:
            expected_value = result_df.iloc[0][expected_result['result_name']]
        assert expected_value == expected_result['result_value']

def load_gcs_dataset(suite_configuration, dataset_config):
    bq_client = bigquery.Client(project=suite_configuration.gcp_project)
    dataset = bq_client.dataset(dataset_config.bq_destination_dataset)
    table_ref = dataset.table(dataset_config.bq_destination_table)
    job_id_prefix = "load_{}_{}".format(dataset_config.bq_destination_dataset, dataset_config.bq_destination_table)
    job_config = bigquery.LoadJobConfig()
    job_config.create_disposition = 'CREATE_IF_NEEDED'
    job_config.schema = _create_bq_schema(dataset_config.bq_schema)
    job_config.skip_leading_rows = 1
    job_config.source_format = 'CSV'
    job_config.write_disposition = 'WRITE_TRUNCATE'
    load_job = bq_client.load_table_from_uri(
        dataset_config.file_location_url, table_ref, job_config=job_config,
        job_id_prefix=job_id_prefix)  # API request
    load_result = load_job.result()

def _create_bq_schema(bq_schema_def):
    bq_schema = []
    for col in bq_schema_def:
        bq_schema.append(bigquery.SchemaField(col["column_name"], col["column_type"]))
    return bq_schema