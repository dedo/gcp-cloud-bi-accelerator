from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

setup(
    name='precocity-pipeline-validator',
    version='0.0.1',
    description='Tool to perform black box validation on data pipelines',
    long_description=readme,
    author='Precocity, LLC',
    author_email='info@precocityllc.com',
    url='https://bitbucket.org/chrisdebracy/gcp-cloud-bi-accelerator',
    packages=find_packages(exclude=('tests', 'docs')),
    include_package_data=True,
    entry_points='''
        [console_scripts]
        dpvalidator=dpvalidator:main
    '''
)