# Data Pipeline Testing Harness

The data pipeline tests are designed to run in a docker container.   Right now, the Dockerfile usines the Alpine distribution.  This is a benefit because the image is small; however, creating the container takes a very long time as Pandas and Numpy have to built from scratch.   

The container can be built using Google Cloud's Container Builder.   Assuming you have `gcloud` installed and are already authenticated, just make sure that the default project is set appropriately.

`gcloud config set project ziptie-ulta-demo`

In order to build the container, you need the service account credentials.  For this project, I'm using the "Data Pipeline Validator" service account.  The JSON key is required during the build as it is added into the container image.   For security reasons, this is *NOT* checked into the repository.  You will need to create a key and place it in this directory with the name `ziptie-ulta-demo-service-account.json`.

Since the build takes so long, you must bump the timeout parameter on the container build--I seriously might switch to using an Ubuntu image because this is **SO** slow.  The command is as follows:

`gcloud container builds submit --timeout=1800 --tag gcr.io/ziptie-ulta-demo/ulta-demo-validator .`

Running the docker container without any parameters will run the tests.   The output is the standard output from pytest.  I will look into generating an HTML report. Execution of this docker container should be merged into the overall CI pipeline. 

To get this working in Jenkins, I did the following:

* Installed Docker on the VM following the instructions here:  https://docs.docker.com/install/linux/docker-ce/debian/#set-up-the-repository
* Added the Jenkins user to the docker group as follows:  `sudo usermod -a -G docker jenkins`
* Used a service account user that was already set up.   Made sure it had the following permissions:
    * Storage Admin
    * Storage Object Viewer
* Exported the JSON key file from the Google Console
* Installed the following plugins in Jenkins:
    * Google OAuth Credentials Plugin
    * Google Container Registry Auth Plugin
    * Docker Pipeline Plugin (was already installed -- I updated it)
* Bounced the VM to get the docker group effectively added to the Jenkins user
* Created a new credential in Jenkins using the JSON key file for the service account
* Created a new Pipeline based project in Jenkins...used the pipeline file in this directory:  `docker_exec_jenkins_pipeline`