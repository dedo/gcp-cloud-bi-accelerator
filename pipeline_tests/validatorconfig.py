import yaml

class ValidatorConfig(object):

    def __init__(self, config_file_path):
        with open(config_file_path, 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
            self.test_suite_name = cfg["test_suite_name"]
            self.gcp_project = cfg["gcp_project"]
            self.file_based_datasets = self._load_file_based_datasets(cfg["file_based_datasets"])
            self.test_definitions = self._load_test_definitions(cfg["test_definitions"])
    
    def _load_file_based_datasets(self, file_datasets):
        the_datasets = []
        for ds in file_datasets:
            the_datasets.append(FileBasedDataSet(ds))
        return the_datasets

    def _load_test_definitions(self, test_defs_raw):
        test_definitions = []
        for test in test_defs_raw:
            test_definitions.append(ValidatorTestDef(test))
        return test_definitions

class FileBasedDataSet(object):
    def __init__(self, yaml_dataset_def):
        self.dataset_name = yaml_dataset_def["dataset_name"]
        self.storage_type = yaml_dataset_def["storage_type"]
        self.file_location_url = yaml_dataset_def["file_location_url"]
        self.file_type = yaml_dataset_def["file_type"]
        self.bq_destination_dataset = yaml_dataset_def["bq_destination_dataset"]
        self.bq_destination_table = yaml_dataset_def["bq_destination_table"]
        self.remove_table_after_test = yaml_dataset_def["remove_table_after_test"]
        self.bq_schema = []
        for col in yaml_dataset_def["bq_schema_def"]:
            self.bq_schema.append(col)


class ValidatorTestDef(object):

    def __init__(self, yaml_test_def):
        self.test_name = yaml_test_def["test_name"]
        self.test_type = yaml_test_def["test_type"]
        if 'bq_dataset' in yaml_test_def:
            self.bq_dataset = yaml_test_def["bq_dataset"]
        if 'bq_table_name' in yaml_test_def:
            self.bq_table_name = yaml_test_def["bq_table_name"]
        if 'bq_query' in yaml_test_def:
            self.bq_query = yaml_test_def["bq_query"]
        self.expected_results = map(self._parse_expected_result, yaml_test_def["expected_results"])
        isValid = self._validate_test_config()

    def _parse_expected_result(self, result):
        expected_result = {}
        expected_result['result_name'] = result["result_name"]
        expected_result['result_type'] = result["result_type"]
        expected_result['result_value'] = result["result_value"]
        return expected_result

    def _validate_test_config(self):
        return True


