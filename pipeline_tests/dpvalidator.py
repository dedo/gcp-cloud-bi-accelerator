#!/usr/bin/env python

import pytest
import sys
import gcp_bq_validator as gcp_bq
from validatorconfig import ValidatorConfig

# Dynamically generate test fixture
def generate_test(test_function, suite_configuration, test_definition):
    def pipeline_dynamic_test():
        test_function(suite_configuration, test_definition)
    return pipeline_dynamic_test

def inject_test(name, test_function, suite_configuration, test_definition):
    test_fn_name = "test_{}".format(name)
    globals()[test_fn_name] = generate_test(test_function, suite_configuration, test_definition)

def load_file_based_datasets(test_config, file_based_datasets):
    for ds in file_based_datasets:
        if ds.storage_type == "gcs":
            print("Loading Data Set: {}".format(ds.dataset_name))
            gcp_bq.load_gcs_dataset(test_config, ds)
        else:
            raise "Unsupported Storage Type"


test_config = ValidatorConfig('retail-demo-pipeline-tests.yml')
print(test_config.file_based_datasets)
load_file_based_datasets(test_config, test_config.file_based_datasets)
for pipeline_test in test_config.test_definitions:
    if pipeline_test.test_type == "bq_row_count":
        the_method = gcp_bq.validate_bq_table_row_count
    elif pipeline_test.test_type == "bq_custom_query":
        the_method = gcp_bq.validate_bq_custom_query
    else:
        raise "Test Type Not Supported"
    inject_test(pipeline_test.test_name, the_method, test_config, pipeline_test)   

def main():
    print("I can validator your data pipelines")
    test_config = ValidatorConfig('retail-demo-pipeline-tests.yml')
    for pipeline_test in test_config.test_definitions:
        print("{}: {}".format(pipeline_test.test_name, pipeline_test.test_type))
        gcp_bq.validate_bq_table_row_count(test_config, pipeline_test)
    
if __name__ == "__main__":
    main() 