# Retail - GCP Data Pipeline Demo Script

## Prep Steps
1. ??
2. ??

## Demo Steps

1. Set the stage with some high-level explanations / descriptions
1. Show overall architecture (*TODO: Need cleaned up diagram*) and source data / model
1. Explain overall data flows and associated pipelines.   Talk about data value chain.  Point out that the reference architecture covers both batch and streaming use cases and that it includes job orchestraton (via airflow), as well as non-functionals -- logging, monitoring via stackdriver.
1. Explain that the components are deployed using Terraform / Ansible
1. Show the code / configuration for one of the batch loads (e.g., customer).  Show the DataDlow code.  Show how monitoring / logging hooks in.  Show error handling.  Explain the Airflow component / configuration.
1. Manually execute the batch job.  Show the results in the final BQ table.  Show the data collected in Stackdriver
1. Demonstrate how we would add a new job in...the idea is not that we write the job from scratch, but more that we demonstrate how it gets deployed and tested.   Ideally, we would deploy the DataDlow job, the Airflow config, the BQ Schema, execute the job, and then run the pipeline tests -- these could be manually triggered
1. Streaming ingest example, with monitoring and error handling (dead letter queue).  Show how data is teed into GCS.