# gcp-cloud-bi-accelerator

## Creating / Updating BQ Schema

Not quite sure how to do updates to the schema yet...what I tried didn't work.  So I'm using these two commands (you need the BQ CLI tool installed):

* Creating the `sales_fact` table with the JSON in the root directory of this project (`bq_sales_schema_def.json`)

`bq mk --table ziptie-ulta-demo:retail_demo_warehouse.sales_fact ./bq_sales_schema_def.json`

* Dropping the table

`bq rm ziptie-ulta-demo:retail_demo_warehouse.sales_fact`

