# Explanation of what it is and what is isn't

### What it is    
 * Reference architectures and pre-integrated components to jumpstart data-focused engagements -- based on best practices and ample experience
 * Examples focused on use of Airflow and DataFlow for batch and streaming data ingest into BigQuery
 * Scripts to performa automated installations / configuration of key elements including GCP components, CI/CD, Airflow, etc.
 * Demonstration of error handling and integration with proper monitoring / logging
 * Black-box data pipeline testing framework - heavily leverages Google Cloud Storage and BigQuery
 * Based on basic retail data sets and common data patterns found in retail

### What it isn't
    * An out of the box product that can be leveraged without technical resources
    * A full-blown, ready to go retail data model with associated canned reports

# The Advantages

* Shifts intial effort from picking pieces and parts to diving straight into the domain-specific development
* Eases the process of ingesting new data sources (batch or real-time)
* Forces the team to include non-functions (e.g., observability) from day one
* Encourages automation and test-driven development by stipulating the techniques in the reference architecture
* Leaves our customers with a solid foundation upon which they can continue to build and evolve quality data pipelines

# High-Level Architecture 
  
Walk through diagram

# Demo

*See demo script*