INSERT `retail_demo_warehouse.sales_fact` (order_id, channel_name, customer_id, customer_number, customer_last_name, customer_first_name, customer_birth_date, customer_gender, customer_marital_status, customer_anniversary, customer_email_address, customer_primary_phone, customer_primary_phone_type, employee_flag, customer_social_handle_1, social_handle_type, household_id, total_order_amount, store_id, store_number, store_name, store_city, store_state, store_postal_code, store_lat, store_long, pos_terminal_id, transaction_date, transaction_number, merchandise_total_ammount, tax_total_ammount, payment_type, employee_id, employee_number, employee_first_name, employee_last_name, employee_type, employee_level, line_items)
select header_flat.*, ARRAY_AGG(line_item_flat) AS line_items
FROM
    (
        select header.order_id, 
        channel.channel_name,
        header.customer_id,
        customer.customer_number,
        customer.last_name as customer_last_name,
        customer.first_name as customer_first_name,
        customer.birth_date as customer_birth_date,
        customer.gender as customer_gender,
        customer.marital_status as customer_marital_status,
        customer.anniversary as customer_anniversary,
        customer.primary_email as customer_email_address,
        customer.primary_phone as customer_primary_phone,
        customer.primary_phone_type as customer_primary_phone_type,
        customer.employee_flag as employee_flag,
        customer.social_handle_1 as customer_social_handle,
        customer.social_handle_1_type as customer_social_handle_type,
        customer.household_id as customer_household_id,
        header.total_amount,
        header.store_id,
        store.store_num as store_number,
        store.store_name as store_name,
        store.store_city as store_city,
        store.store_state as store_state,
        store.store_postal_code as store_postal_code,
        store.latitude as store_lat,
        store.longitude as store_long,
        header.pos_terminal_id as pos_terminal_id,
        header.transaction_date as transaction_date,
        header.transaction_number as transaction_number,
        header.merchandise_amount as merchandise_total_amount,
        header.tax_amount as tax_total_amount,
        header.payment_type as payment_type,
        employee.employee_id as employee_id,
        employee.employee_number as employee_number,
        employee.employee_first_name as employee_first_name,
        employee.employee_last_name as employee_last_name,
        employee.employee_type as employee_type,
        employee.employee_level as employee_level
        FROM `retail_demo_warehouse.sales_header` header
        LEFT JOIN `retail_demo_warehouse.channel` channel
        ON header.channel_id = channel.channel_id
        LEFT JOIN `retail_demo_warehouse.customer` customer
        ON header.customer_id = customer.customer_id
        LEFT JOIN `retail_demo_warehouse.store` store
        ON header.store_id = store.store_id
        LEFT JOIN `retail_demo_warehouse.employee` employee
        ON header.primary_associate_id = employee.employee_id
        WHERE header.created_timestamp >= @start_ts AND header.created_timestamp < TIMESTAMP_ADD(@start_ts, INTERVAL 5 MINUTE)
    ) header_flat
    INNER JOIN (
        SELECT
        li.order_id as order_id, 
        li.line_item_id as line_item_id,
        li.item_id as item_id,
        vendor.vendor_name as vendor_name,
        division.division_name as division_name,
        department.department_name as department_name,
        classification.classification_name as classification_name,
        sub_class.sub_classification_name as sub_classification_name,
        style.style_name as style_name,
        item.sku as sku,
        item.color as color,
        item.size as size,
        item.short_description as product_name,
        item.current_price as current_item_price,
        li.transaction_type as transaction_type,
        li.quantity as quantity,
        li.item_price_each as unit_price_each,
        li.item_tax_each as unit_tax_each,
        li.markdown_each as markdown_amount_each,
        li.freight_each as freight_amount_each,
        li.duty_each as duty_amount_each,
        li.gift_wrap_flag as gift_wrap_flag,
        li.item_status as item_status,
        li.item_status_as_of_date as item_status_as_of_date,
        li.ship_date as ship_date,
        li.promotion_code as promotion_code,
        li.gift_registry_number as gift_registry_number
        FROM `retail_demo_warehouse.sales_line_item` li
        LEFT JOIN `retail_demo_warehouse.item` item
        ON li.item_id = item.item_id
        LEFT JOIN `retail_demo_warehouse.vendor` vendor
        ON item.vendor_id = vendor.vendor_id
        LEFT JOIN `retail_demo_warehouse.division` division
        ON item.division_id = division.division_id
        LEFT JOIN `retail_demo_warehouse.department` department
        ON item.department_id = department.department_id
        LEFT JOIN `retail_demo_warehouse.classification` classification
        ON item.classification_id = classification.classification_id
        LEFT JOIN `retail_demo_warehouse.sub_classification` sub_class
        ON item.sub_classification_id = sub_class.sub_classification_id
        LEFT JOIN `retail_demo_warehouse.style` style
        ON item.style_id = style.style_id
    ) line_item_flat
ON header_flat.order_id = line_item_flat.order_id
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
25,26,27,28,29,30,31,32,33,34,35,36,37,38;
