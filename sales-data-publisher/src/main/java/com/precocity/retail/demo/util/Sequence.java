package com.precocity.retail.demo.util;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import java.util.concurrent.atomic.AtomicInteger;

public class Sequence {

    private final static int TOTAL_SALES = 50000;
    private final static String BUCKET_NAME = "ulta-keys";
    private final static String SEQ_FILE_NAME = "sales-sequence";

    private static AtomicInteger id;
    private static Storage storage;
    private static Bucket bucket;

    static {
        storage = StorageOptions.getDefaultInstance().getService();
        bucket = storage.get(BUCKET_NAME);
        Blob sequence =  bucket.get(SEQ_FILE_NAME);
        if (sequence != null) {
            id = new AtomicInteger(Integer.parseInt(new String(sequence.getContent())));
        } else {
            id = new AtomicInteger(-1);
        }
    }

    public static int getNextVal() {
        return id.incrementAndGet() % TOTAL_SALES + 1;
    }

    public static void save() {
        Blob sequence =  bucket.get(SEQ_FILE_NAME);
        if (sequence != null) {
            sequence.delete();
        }
        bucket.create(SEQ_FILE_NAME, id.toString().getBytes());
    }
}