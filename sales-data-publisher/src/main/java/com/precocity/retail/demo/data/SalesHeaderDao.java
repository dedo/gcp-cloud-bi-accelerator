package com.precocity.retail.demo.data;

import com.precocity.retail.demo.model.SalesHeader;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class SalesHeaderDao extends BaseDao {
    public static String getSalesHeader(final int orderId, final long txTimeMillis) {
        SalesHeader sh = SalesHeader.findFirst("order_id = ?", orderId);
        Timestamp txTime = new Timestamp(txTimeMillis);
        Date txDate = new Date(txTimeMillis);
        String txDateStr = new SimpleDateFormat("yyyy-MM-dd").format(txDate);
        sh.set("order_id", txTimeMillis + orderId);
        sh.set("transaction_date", txDateStr);
        sh.set("created_timestamp", txTime);
        sh.set("updated_timestamp", txTime);
        return sh.toJson(false);
    }
}