package com.precocity.retail.demo.stream;

import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;

import java.util.concurrent.ExecutionException;

public class SalesHeaderPublisher {

    private static Publisher salesHeaderPublisher;

    static {
        try {
            salesHeaderPublisher = Publisher.newBuilder("projects/ziptie-ulta-demo/topics/sales-header").build();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void publish(final String message) {
        PubsubMessage psm = PubsubMessage.newBuilder().setData(ByteString.copyFromUtf8(message)).build();
        try {
			//wait till the publish is complete
            salesHeaderPublisher.publish(psm).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

}
