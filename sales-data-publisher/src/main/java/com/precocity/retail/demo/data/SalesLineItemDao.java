package com.precocity.retail.demo.data;

import com.precocity.retail.demo.model.SalesLineItem;
import org.javalite.activejdbc.LazyList;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SalesLineItemDao extends BaseDao {
    public static List<String> getSalesLineItems(final int orderId, final long txTimeMillis) {
        LazyList<SalesLineItem> salesLineItems = SalesLineItem.find("order_id = ?", orderId);
        List<String> messages = new ArrayList<String>();
        Timestamp txTime = new Timestamp(txTimeMillis);
        for(SalesLineItem sli : salesLineItems) {
            sli.set("order_id", txTimeMillis + orderId);
            sli.set("created_timestamp", txTime);
            sli.set("updated_timestamp", txTime);
            messages.add(sli.toJson(false));
        }
        return messages;
    }
}