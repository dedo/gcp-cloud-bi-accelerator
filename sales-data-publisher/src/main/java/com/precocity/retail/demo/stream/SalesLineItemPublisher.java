package com.precocity.retail.demo.stream;

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SalesLineItemPublisher {

    private static Publisher salesLineItemPublisher;

    static {
        try {
            salesLineItemPublisher = Publisher.newBuilder("projects/ziptie-ulta-demo/topics/sales-line-item").build();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void publish(final List<String> messages) {
        List<ApiFuture<String>> responses = new ArrayList<ApiFuture<String>>();
        for(String message: messages) {
            PubsubMessage psm = PubsubMessage.newBuilder().setData(ByteString.copyFromUtf8(message)).build();
            responses.add(salesLineItemPublisher.publish(psm));
            try {
                for(ApiFuture<String> response : responses) {
                	//wait til all the current messages are published
				    response.get();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

}
