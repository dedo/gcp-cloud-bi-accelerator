package com.precocity.retail.demo.data;

import org.javalite.activejdbc.Base;

public abstract class BaseDao {
    static {
        String jdbcUrl = String.format(
                "jdbc:mysql://google/%s?cloudSqlInstance=%s" + "&socketFactory=com.google.cloud.sql.mysql.SocketFactory&useSSL=false",
                "ulta_retail_demo_data",
                "ziptie-ulta-demo:us-central1:ulta-demo-mysql");
        //TODO get rid of the password
        Base.open("com.mysql.jdbc.Driver", jdbcUrl, "ulta", "ulta");
    }
}
