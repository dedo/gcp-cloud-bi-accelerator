package com.precocity.retail.demo;

import com.precocity.retail.demo.data.SalesHeaderDao;
import com.precocity.retail.demo.data.SalesLineItemDao;
import com.precocity.retail.demo.stream.SalesHeaderPublisher;
import com.precocity.retail.demo.stream.SalesLineItemPublisher;
import com.precocity.retail.demo.util.Sequence;

import java.util.List;

public class SalesDataPublisher implements Runnable {

    private boolean running;
    private boolean publishing;

    public void run() {
        while(true) {
            publishing = true;
            int orderId = Sequence.getNextVal();
            long currentTime = System.currentTimeMillis();
            String message = SalesHeaderDao.getSalesHeader(orderId, currentTime);
            List<String> messages = SalesLineItemDao.getSalesLineItems(orderId, currentTime);
            SalesHeaderPublisher.publish(message);
            SalesLineItemPublisher.publish(messages);
            publishing = false;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //nothing
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isPublishing() {
        return publishing;
    }

    public void setPublishing(boolean publishing) {
        this.publishing = publishing;
    }

    public static void main(String[] args) {
        final SalesDataPublisher sdg = new SalesDataPublisher();
        final Thread sdgThread = new Thread(sdg);

        sdgThread.start();
        sdg.setRunning(true);

        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                while(sdg.isPublishing()){
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                sdgThread.interrupt();
                sdg.setRunning(false);
                try {
                    mainThread.join();
                } catch (InterruptedException ex) {
                    //terminate
                }
            }
        });

        while (sdg.isRunning()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                //terminate
            }
        }

        //last step to save sequence
        System.out.println("Saving Sequence..");
        Sequence.save();
    }
}
