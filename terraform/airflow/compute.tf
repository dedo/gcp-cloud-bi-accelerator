# Create a new instance
resource "google_compute_instance" "debian" {
   name = "airflow"

#   tags = ["default_allow_ssh"]

   machine_type = "n1-standard-2"
   zone = "us-central1-c"   
   boot_disk {
      initialize_params {
      image = "debian-9-stretch-v20180510"
   }
}

network_interface {
   network = "default"
   access_config {}
}

service_account {
   scopes = ["userinfo-email", "compute-ro", "storage-ro"]
   }
}



#resource "google_compute_network" "opsmgr" {
#  name                    = "opsmgr"
#  auto_create_subnetworks = "false"
#}

#resource "google_compute_firewall" "pcf-lb" {
#  name          = "pcf-lb"
#  network       = "${google_compute_network.opsmgr.self_link}"
#  source_ranges = ["0.0.0.0/0"]

#  allow {
#    protocol = "tcp"
#    ports    = ["22", "443", "2222", "8080"]
#  }
#
#  target_tags = ["pcf-lb"]
#}