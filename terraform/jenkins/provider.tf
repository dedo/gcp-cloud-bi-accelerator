# Specify the provider (GCP, AWS, Azure)
provider "google" {
	credentials = "${file("../../service-account/ziptie-ulta-demo-7cf293451572.json")}"
	project = "ziptie-ulta-demo"
	region = "us-central1"
}
